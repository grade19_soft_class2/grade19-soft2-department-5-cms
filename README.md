# 19级软件2班5部CMS项目

# 一、浏览器端（用户端）展示效果如下：
![](https://gitee.com/myhfw003/jixundiyigexiangmucmsxuqiu/raw/master/imgs/index.png)
![](https://gitee.com/myhfw003/jixundiyigexiangmucmsxuqiu/raw/master/imgs/content.png)

# 二、后台界面需要的功能大致如下：
1. 管理网站的信息，包括但不限于公司名称、关于我们、招聘信息等
2. 管理轮播图
3. 文章栏目（分类）管理
4. 文章管理，包括但不限于文章标题、文章简介、文章封面图、文章内容（可能包括文字、图片、音视频）、文章的阅读数量、评论数量、点赞数量、编辑是否推荐
5. 网站下方关于微信公众号二维码、微信群二维码、QQ二维码的管理
6. 关于网站ICP、公安备案信息管理
7. 后台管理系统自身的一些管理，包括但不限于权限管理、用户管理、用户是否删除、用户是否禁用等

# 三、技术栈：
1. 前端：Vue/CLI、Element-UI
2. 后端：.Net Core
3. 数据库：PostgreSql

# 四、人员分配：
 - 黄炎榕：前端页面（完成之后辅助后端设计及api接口的实现）

 - 黄宇煌：后端API接口（使用 .net core ）

 - 李泽：数据库设计（使用 PowerDesign ，完成后辅助后端API接口）

 - 张林红：后台管理界面原型设计（需实际点，要可以做得到的）

 - 许梦婷：后台管理界面原型设计（需实际点，要可以做得到的）

 - 梁艺缤：数据库设计（使用 PowerDesign ，完成后辅助后端API接口）
