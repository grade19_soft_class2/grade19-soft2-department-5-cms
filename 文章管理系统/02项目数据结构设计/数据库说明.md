
># 数据库信息说明


>## 1.基础实体信息表 (BaseEntity)

| 参数名        | 类型      |  说明           |
| ------------ | -------- | ---------------- |
| Id           | int      | 用户Id {主键}     |
| IsActived    | bool     | 激活状态          |
| IsDeleted    | bool     | 删除状态          |
| CrestedTime  | DateTime | 创建时间          |
| UpdatedTime  | DateTime | 更新时间          |
| Remarks      | string   | 备注              |

### 说明：所有表继承基础实体信息表 (BaseEntity)，每个表都有基础实体信息表的字段


>## 2.用户信息表 (Users)

|    名称           |   类型  |  说明                              |
|------------------|--------|------------------------------------ |
| username         | string | 用户名                               |
| password         | string | 密码                                 |
| Nickname         | string | 昵称                                 |
| AvatarId         | int    | 头像Id                               |
| TelephoneNumber  | string | 手机号码                             |
| Permissions      | byte   | 用户权限(0--管理员用户，1--普通用户)   |
| Email            | string | 邮箱                                 |



>## 3.文章信息表 (Articles)

|    名称              |   类型     |  说明         |
|---------------------|---------- |--------------- |
| UserId              | int       |用户Id           |
| ArticleTitle        | string    |文章标题         |
| ArticleContent      | string    |文章内容         |
| CategoryId          | int       | 文件栏目Id      |
| ArticleCategory     | string    | 文件栏目名称    |
| Author              | string    | 作者           |
| ThumbnailId         | int       | 文章缩略图Id    |
| ArticleViews        | int       | 浏览量          |
| ArticlePraise       | int       | 点赞数          |
| ArticleCommentCount | int       | 评论总量        |



>## 4.文章栏目信息表 (ArticleCategories)

|    名称        |   类型    |   说明              |
|---------------|--------- |--------------------- |
| Category      | string   | 文章栏目              |
| ArticlesCount | int      | 该栏目下文章数量总和   |
| Creator       | string   | 栏目创建人            |



>## 5.评论信息表 (Comments)

|    名称          |   类型    |  说明          |
|-----------------|--------- |---------------- |
| FromArticleId   | int      | 评论文章Id       |
| CommentNickname | string   | 评论文章Id       |
| CommentContent  | string   | 评论内容         |



>## 6.权限信息表 (Permissions)

|    名称           |   类型    |  说明             |
|----------------- |----------|------------------- |
| PermissionsName  | string   | 用户权限名          |
| UsersCount       | int      | 权限下用户数量总和   |    
| Creator          | string   | 权限创建人          |



>## 7.公安备案信息表 (WebsiteInfo)

|    名称     |   类型    |  说明         |
|--------------|--------|--------------- |
| WebsiteName  | string | 网站名称        |
| DomainName   | string | 域名            |
| ICPNumber    | string | ICP备案         |
| PoliceNumber | string | 公安备案        |



>## 8.轮播图信息表 (Slideshows)

|    名称     |   类型    |  说明           |
|-------------|---------|----------------- |
| PicId       | int     | 图片Id            |


>## 9.二维码信息表 (QRCodes)

|    名称     |   类型    |  说明            |
|-------------|---------|------------------ |
| PicId       | int     | 图片Id            |



>## 10.图片上传信息表 (UploadFileInfo)

|    名称         |   类型    |  说明
|--------------- |----------|------------------------------ |
| OriginName     | string   | 原始名称（含扩展名）            |
| CurrentName    | string   | 现在的名称（随机名称，含扩展名   |
| RelativePath   | string   | 相对地址                       |
| ContentType    | string   | 文件类型                       |



>## 11.点赞信息表 （Praises）

|    名称          |   类型   |  说明                           |
|-----------------|---------|--------------------------------- |
| FromUserId      | int     | 点赞用户Id                        |
| FromArticleId   | int     | 点赞文章Id                        |
| PraiseStatus    | byte    | 点赞状态(0--取消赞/1--有效赞)      |





